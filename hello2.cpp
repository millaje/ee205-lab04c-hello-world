///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
///
/// @file hello2.cpp
/// @version 1.0
///
/// 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>    // The object-oriented version of stdio.h

int main() {
   std::cout << "Hello World!" << std::endl;
  return 0;
}
