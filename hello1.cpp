///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 04c - Hello World
///
///
/// @file hello1.cpp
/// @version 1.0
///
/// 
///////////////////////////////////////////////////////////////////////////////

#include <iostream>    // The object-oriented version of stdio.h

using namespace std;

int main() {
  cout << "Hello World!" << endl;
  return 0;
}
